import { NgModule } from '@angular/core';
import {AppComponent} from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AppMaterialThemeModule} from './app-material-theme/app-material-theme.module';
import {AppRouterModule} from './app-router.module';
import {AuthenticationService} from './services/authentication.service';
import {CanActivateRouteGuard} from './can-activate-route.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from './jwt-interceptor.service';
import { NotesService} from './services/notes.service';
import {BrowserModule} from '@angular/platform-browser';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatCardModule,MatToolbarModule,MatSelectModule} from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {MatExpansionModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common'
@NgModule({
  declarations: [ AppComponent,HeaderComponent],
  imports: [AppMaterialThemeModule,AppRouterModule,HttpModule,HttpClientModule,BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    FormsModule,
    HttpModule,
    MatExpansionModule,
    MatSelectModule,
    ReactiveFormsModule,
    CommonModule],
  providers: [AuthenticationService,CanActivateRouteGuard,NotesService,
  {
  	provide : HTTP_INTERCEPTORS,
  	useClass : JwtInterceptor,
  	multi : true
  } ],
  bootstrap: [ AppComponent]
})

export class AppModule { }
