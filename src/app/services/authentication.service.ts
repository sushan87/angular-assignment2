import { Injectable } from '@angular/core';
import {User} from '../user';
import 'rxjs/add/observable/throw';
import {HttpClientModule,HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Response,RequestOptions,Http,Headers,URLSearchParams} from '@angular/http';
import {HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpErrorResponse} from '@angular/common/http'
import { Observable,BehaviorSubject} from 'rxjs';
@Injectable()
export class AuthenticationService {

  authenticateUserUrl = "http://localhost:3000/auth/v1";
  isUserAuthenticatedUrl = "http://localhost:3000/auth/v1/isAuthenticated";

  isUserAuthenticate= new BehaviorSubject<boolean>(false);
  user:User;
  headers : Headers;
  options: RequestOptions;
  constructor(private http:HttpClient,public router : Router) {
    this.headers=new Headers({});
    this.options=new RequestOptions({headers:this.headers});
  }
  private extractData(res: Response){
    
    alert('res'+res.toString)
    return res;
  }
  private extractDataBoolean(res: Response) : boolean {
    let value :string = res.toString();
    let body : boolean = value.toLowerCase() == 'true'?true:false;
    return body;
  }
  authenticateUser(username,password): Observable<Object> {
    this.user =  {username:username,password:password};
    return this.http.post(this.authenticateUserUrl, {username:username,password:password}).map(this.extractData).catch(this.handleErrorObservable);
    
  }

  setBearerToken(token) {
     localStorage.setItem('bearerToken', token);
  }

  getBearerToken() {
    localStorage.getItem('bearerToken');
  }

  isUserAuthenticated(token): Promise<boolean> {
      

      return this.http.post(this.isUserAuthenticatedUrl,{}).toPromise().then(this.extractDataBoolean).catch(this.handleError);
  }
  private checkUserAuthenticated(){

  }
  private handleErrorObservable (error: HttpErrorResponse | any) {
    console.error(error.message || error);
    
    return Observable.throw(new HttpErrorResponse(error));
  }
  private handleError(error: HttpErrorResponse | any) {
    console.error(error.message || error);
    
    return Promise.reject(error.message || error);
  }
}