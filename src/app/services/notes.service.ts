import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Note } from '../note';
import 'rxjs/add/observable/throw';
import {HttpClient} from '@angular/common/http';
import {Response,RequestOptions} from '@angular/http';
import {HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpErrorResponse} from '@angular/common/http'
@Injectable()
export class NotesService {
	url = "http://localhost:3000/notes";
  constructor(private http:HttpClient) { }
  
  private extractData(res: Response) {
    
    let body = res;
    return body;
  }
  
  
  getNotes(): Observable<Array<Note>> {
    return this.http.get(this.url)
    .map(this.extractData)
    .catch(this.handleErrorObservable);
    ;
  }

  addNote(note: Note): Observable<Note> {
    
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.url, note)
    .map(this.extractData)
    .catch(this.handleErrorObservable);

  }
  
  private handleErrorObservable (error: HttpErrorResponse | any) {
    console.error(error.message || error);
    
    return Observable.throw(new HttpErrorResponse(error));
  }

}
