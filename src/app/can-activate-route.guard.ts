import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthenticationService} from './services/authentication.service';
import {RouterService} from './services/router.service'

@Injectable()
export class CanActivateRouteGuard implements CanActivate {

  constructor(private auth : AuthenticationService, private routerServive : RouterService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  	let authToken = this.auth.getBearerToken();
  	let isAuthenticated : boolean = false;
  	var authenticationPromise = Promise.resolve(this.auth.isUserAuthenticated(authToken)).then(function(value){
  		isAuthenticated = value;
  	});
    if(!isAuthenticated){
    	 this.routerServive.routeToLogin();
    	 return false;
    }
    this.routerServive.routeToDashboard();
    return true;
  }
}
