import { Component,OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators,FormBuilder,AbstractControl } from '@angular/forms';
import {AuthenticationService} from '../services/authentication.service';
import {HttpErrorResponse} from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
	errorMessage : String;
	loginForm:FormGroup;
    username = new FormControl('',Validators.required);
    password = new FormControl('',Validators.required);
    token:String;
    ngOnInit(){
    	this.createForm();
    }
    private createForm(){
    	this.loginForm= this.fb.group({username : this.username,password: this.password});
    }
    constructor(private authservice: AuthenticationService,private fb:FormBuilder ){}
    loginSubmit() {
    	if(this.loginForm.valid){
    		let username = this.loginForm.value.username;
    		let password = this.loginForm.value.password;
    		this.authservice.authenticateUser(username,password).subscribe(
    			token=>this.token
    		);
    		alert('token'+this.token)
    		if(this.token != null){
    			this.authservice.setBearerToken(this.token);
    		}
    		this.resetForm();
    	}
    }
    handleErrorResponse(error: HttpErrorResponse): void {
   // error to display when it is failure

   if (error.status === 404) {
     this.errorMessage = `Http failure response for ${error.url}: 404 Not Found`;
   } else {
     this.errorMessage = 'An error occurred:' + error.error.message;
   }
 }
    resetForm(){
    	let control : AbstractControl = null;
    	this.loginForm.reset();
    	Object.keys(this.loginForm.controls).forEach(
    		(name)=>{
    			control = this.loginForm.controls[name];
    			control.setErrors(null);

    		});
    }
}
