import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { Observable } from 'rxjs';
import {NgModule} from '@angular/core';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatCardModule,MatToolbarModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { NotesService } from '../services/notes.service';
import { Note } from '../note';
import {FormGroup,FormControl,Validators,FormArray} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
title = 'A note taking app';
  observableNotes: Observable<Note[]>
  notes: Note[];
  noteArray:Note[];
  errMessage: String;
  errorMessage : String;
  id: String;
  noteTitle: String;
  text: String;
  note = new Note();
  constructor(private noteService: NotesService) { }

  ngOnInit(): void {
   
   
    this.fetchNotes();
  }
  fetchNotes():void{
    this.noteService.getNotes().subscribe(
      notes => this.notes = notes,
      error =>  this.handleErrorResponse(error));
    //error => this.errorMessage = <any>error);

  }
  addNotes():void{
     this.errMessage = '';

   
    if(this.validateNote()){
     this.noteService.addNote(this.note).subscribe( note=> {
      
      
      this.reset();   
      this.noteTitle= note.title;   
      this.text= note.text;    
      this.notes.push(note);

      
    },
    //error => this.errorMessage = <any>error);
    error=>this.handleErrorResponse(error));
   }else{
    
    }
  }
  validateNote(): boolean {
   if (this.note.title === undefined || this.note.title === '' ||
       !this.note.title.trim() ||this.note.text === undefined ||
       this.note.text === '' || !this.note.text.trim()
       ) {
         // add the error message when any field is empty
     this.errMessage = 'Title and Text both are required fields';
     return false;
   }
   return true;
 }
 handleErrorResponse(error: HttpErrorResponse): void {
   // error to display when it is failure

   if (error.status === 404) {
     this.errMessage = `Http failure response for ${error.url}: 404 Not Found`;
   } else {
     this.errMessage = 'An error occurred:' + error.error.message;
   }
 }
  private reset() {
    this.note.id = null;    
    this.note.title= '';
    this.note.text ='';
    this.errMessage = null;
    this.errorMessage = null;
    this.id= null;
    this.noteTitle= null;
    this.text= null;
  }

}
