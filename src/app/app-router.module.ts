
import {RouterModule,Routes} from '@angular/router'



import {AppComponent} from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AppMaterialThemeModule} from './app-material-theme/app-material-theme.module';

import {AuthenticationService} from './services/authentication.service';
import {CanActivateRouteGuard} from './can-activate-route.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from './jwt-interceptor.service';
import { NotesService} from './services/notes.service';
import {BrowserModule} from '@angular/platform-browser';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatCardModule,MatToolbarModule,MatSelectModule} from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {MatExpansionModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common'
import {NgModule} from '@angular/core';
import {RouterService} from './services/router.service'
const appRoutes : Routes = [
{
	path : '',
	canActivate : [CanActivateRouteGuard],
	component :DashboardComponent
},{
    path : 'login',
	component :LoginComponent

}
]
@NgModule({
	imports : [HttpClientModule,

    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    FormsModule,
    HttpModule,
    MatExpansionModule,
    MatSelectModule,
    ReactiveFormsModule,
    CommonModule,CommonModule,RouterModule.forRoot(appRoutes, {enableTracing : true})],
	declarations : [DashboardComponent,LoginComponent],
	exports : [RouterModule,DashboardComponent,LoginComponent],
	providers : [RouterService]

})

export class AppRouterModule{}